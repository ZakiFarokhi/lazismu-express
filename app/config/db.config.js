const Sequelize = require("sequelize");
const sequelize = new Sequelize(
  "lazismus_express",
  "root",
  "",
  {
    host: "localhost",
    dialect: "mysql",
    operatorsAliases: "0",
    timezone: "+07:00", //indonesia GMT +7
    logging: false,
    dialectOptions: {
      encrypt: false,
    },
  }
);

const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.proposal = require("../model/proposal.model")(sequelize, Sequelize);
db.user = require("../model/user.model")(sequelize, Sequelize)

db.user.hasMany(db.proposal);
db.proposal.belongsTo(db.user)
module.exports = db;
