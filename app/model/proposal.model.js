module.exports = (sequelize, Sequelize) => {
    return sequelize.define('proposal', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        nama: {
            type: Sequelize.STRING(50)
        },
        alamat: {
            type: Sequelize.STRING
        },
        nomorhp: {
            type: Sequelize.STRING
        },
        nominal: {
            type: Sequelize.STRING
        },
        keterangan: {
            type: Sequelize.STRING
        },
        image: {
            type: Sequelize.STRING
        },
        status: {
            type: Sequelize.BOOLEAN
        },
        imageLocal: {
            type: Sequelize.STRING
        }
    })
}