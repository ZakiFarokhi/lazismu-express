module.exports = (sequelize, Sequelize) => {
    return sequelize.define('user', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        nama: {
            type: Sequelize.STRING(50)
        },
        email: {
            type: Sequelize.STRING(100)
        },
        password: {
            type: Sequelize.STRING(100)
        },
        nomorhp: {
            type: Sequelize.STRING(20)
        },
        nomorktp: {
            type: Sequelize.STRING, 
            uniqueKey:true
        }
    })
}