const { proposal } = require("../config/db.config");

exports.create = async (req, res) => {
  try {
    const { nama, alamat, nomorhp, nominal, keterangan, image, imageLocal } = req.body;
    const Create = await proposal.create({
      nama,
      alamat,
      nomorhp,
      nominal,
      keterangan,
      image,
      status : false,
      imageLocal
    });
    if (!Create) {
      res.status(400).json({
        success: false,
        message: "proposal not created",
        data: "error",
      });
    }
    res
      .status(200)
      .json({ success: true, message: "proposal created", data: Create });
  } catch (err) {
    res
      .status(400)
      .json({ success: false, message: "proposal not created with error", data: req.file.path });
  }
};

exports.readAll = async (req, res) => {
  try {
    const Read = await proposal.findAll({
      include: [{ all: true, nested: false }],
    });
    if (!Read) {
      res.status(400).json({
        success: false,
        message: "cannot retrieved proposal",
        data: "error",
      });
    }
    res
      .status(200)
      .json({ success: true, message: "proposal retrieved", data: Read });
  } catch (error) {
    res
      .status(400)
      .json({ success: false, message: "proposal not created", data: error });
  }
};

exports.readOne = async (req, res) => {
  try {
    const Read = await proposal.findOne({
      where: {id:req.query.id},
      include: [{ all: true, nested: false }],
    });
    if (!Read) {
      res.status(400).json({
        success: false,
        message: "cannot retrieved proposal",
        data: "error",
      });
    }
    res
      .status(200)
      .json({ success: true, message: "proposal retrieved", data: Read });
  } catch (error) {
    res
      .status(400)
      .json({ success: false, message: "proposal not created", data: error });
  }
};

exports.find = async (req, res) => {
  try{
    const find = await proposal.findAll({
      where: {userId: req.query.userId}
    })
    if(!find){
      res.status(400)
      .json({success: false, message:"proposal not found", data: null})
    }
    res.status(200)
      .json({success: true, message:"proposal retrieved", data: find })
  }catch(error){
    res
    .status(400)
    .json({ success: false, message: "database error", data: error });
  }
}
