const multer = require("multer");
module.exports = function (app) {
  const controller = require("../controller/proposal.controller");
  const date = new Date();
  const monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  const storage = multer.diskStorage({
    destination: function (req, res, cb) {
      cb(null, `${process.cwd()}/upload`);
    },
    filename: function (req, file, cb) {
      cb(
        null,
        `${date.getDate()}${
        monthNames[date.getMonth()]
        }${date.getFullYear()}_${date.getHours()}${
        (date.getMinutes() < 10 ? "0" : "") + date.getMinutes()
        }.jpg`
      );
    },
  });
  const upload = multer({ storage: storage });

  app.post("/proposal/upload", upload.single("image"), (req, res, next) => {
    return res.status(200).json({ success: true, message: "File uploaded", data: req.file.path })
  })

  app.post("/proposal/create/", controller.create);

  app.get("/proposals/read/", controller.readAll);

  app.get("/proposal/read", controller.readOne);

  app.get("/proposal/find", controller.find);
};
