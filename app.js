const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cors = require("cors");
const db = require("./app/config/db.config");
require("dotenv").config();
app.use(bodyParser.json());
app.use(cors());

app.listen(3131, function () {
  console.log("connected");
});
db.sequelize.sync({alter:true});
app.get("/", function (req, res) {
  res.json({
    success: true,
  });
});
app.get("/connect",  function (req, res) {
  try {
    const connect =  db.sequelize.authenticate();
    res.status(200).json({
      success: true,
      response: connect,
      message: "connected",
    });
  } catch (error) {
    res.json({
      unsuccess: error,
    });
  }
});

require("./app/router/proposal.router")(app);
require("./app/router/user.router")(app)
